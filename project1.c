/*===================================CPEG222====================================
 * Program:		project1.c
 * Authors: 	Galen Nare
 * Date: 		09/09/2020
 * Description: 
 * Input: Button press
 * Output: LED is turned on and off one by one. Pressing the button when LD7 is
 * on increases the score by one and updates the LCD display. Missing the button
 * press ends the game.
==============================================================================*/
/*------------------ Board system settings. PLEASE DO NOT MODIFY THIS PART ----------*/
#ifndef _SUPPRESS_PLIB_WARNING          //suppress the plib warning during compiling
    #define _SUPPRESS_PLIB_WARNING      
#endif
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config POSCMOD = XT             // Primary Oscillator Configuration (XT osc mode)
#pragma config FPBDIV = DIV_8           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/8)
/*----------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <xc.h>   // Microchip XC processor header which links to the PIC32MX370512L header
#include "config.h" // Basys MX3 configuration header
#include "lcd.h"

/* --------------------------- Forward Declarations-------------------------- */
void delay_ms(int);
void lcd_display_start();
void lcd_display_update();
void lcd_print(char*);

/* -------------------------- Definitions------------------------------------ */
#define SYS_FREQ    (80000000L) // 80MHz system clock
int LD_position = 0; // Initially turn on LD0, you can also do LD_position = 1;
int buttonLock = 0; // Variable to "lock" the button
int delay = 50; // A delay variable of 20 ms

int mode = 1; // 1 = Initial | 2 = Ball in Motion | 3 = Hit or Die | 4 = Game Over
int score = 0;

/* ----------------------------- Main --------------------------------------- */
int main(void) {
    /*-------------------- Port and State Initialization -------------------------*/
    DDPCONbits.JTAGEN = 0; // Statement is required to use Pin RA0 as IO
    TRISA &= 0xFF00; // Set Port A bits 0~7 to 0, i.e., LD 0~7 are configured as digital output pins
    TRISFbits.TRISF0 = 1; // Set RF0 to 1, i.e., BTNC is configured as input  
    LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
    
    int btnC_val; // Variable to read button C
    int cycleCounter = 0; // Clock cycle counter to time the LEDs without blocking the program
    
    LCD_Init(); // Initialize the LCD
    lcd_display_start();

    while (1) {
        /*-------------------- Main logic and actions loop --------------------------*/
        btnC_val = PORTFbits.RF0; // read BTNC status
        
        if (mode == 2 || mode == 3) {
            if (LD_position != 0x80) { // if LD7 is not on
                if (cycleCounter >= 100000) {
                    if (LD_position == 0) {
                        LD_position = 1; // Turn on LD0 if none are on
                    } else {
                        LD_position <<= 1; // Turn on left LD and turn off current one
                    }
                    cycleCounter = 0;
                }
            } else { // LD7 is on, start over
                mode = 3;
                if (cycleCounter >= 100000) {
                    LD_position = 0; // Reset to LD0
                    mode = 4;
                }
            }
        }
        
        cycleCounter++; // Increase the counter
        LATA = LD_position; //sets the value for all 8 LEDs
        

        if (btnC_val && !buttonLock) // Actions when central button is pressed
        {
            delay_ms(delay); // wait 20ms, or you can change the value of delay
            // to make the button more/less sensitive
            buttonLock = 1;
            
            switch (mode) {
                case 1: // Reset the game
                    mode = 2;
                    cycleCounter = 100000;
                    score = 0;
                    lcd_display_update();
                    break;
                case 2: // Pressing the button doesn't do anything if LD7 is not lit
                    // Do nothing
                    break;
                case 3: // Reset to LD0 and add 1 to score
                    mode = 2;
                    score++;
                    LD_position = 0; // Reset to LD0
                    cycleCounter = 0;
                    lcd_display_update();
                    break;
                case 4: // Game over, press BTNC to reset
                    mode = 1;
                    LCD_DisplayClear();
                    lcd_display_start();
                    break;
                default:
                    mode = 1;
                    LCD_DisplayClear();
                    lcd_display_start();
                    break;
            }
            
        } else if (buttonLock && !btnC_val) { // Actions when the central button is released
            delay_ms(delay); // wait 20ms, or you can change the value of delay
            // to make the button more/less sensitive
            buttonLock = 0; //unlock buttons to allow further press being recognized
        }
    }
    /*--------------------- Action and logic end ---------------------------------*/
}

void lcd_print(char* str) {
    LCD_DisplayClear();
    LCD_WriteStringAtPos(str, 0, 0);
}

void lcd_display_start() {
    LCD_WriteStringAtPos("    Welcome", 0, 0); //Display "Welcome" at line 0, position 4
    LCD_WriteStringAtPos("Press BTNC", 1, 0); //Display "Press BTNC" at line 1, position 0
}

void lcd_display_update() {
    char scoreStr[40];
    sprintf(scoreStr, "Hit %d time(s)", score);
    LCD_DisplayClear();
    LCD_WriteStringAtPos("    Project 1", 0, 0);
    LCD_WriteStringAtPos(scoreStr, 1, 0);
}

/* ----------------------------------------------------------------------------- 
 **	delay_ms
 **	Parameters:
 **		ms - amount of milliseconds to delay (based on 80 MHz SSCLK)
 **	Return Value:
 **		none
 **	Description:
 **		Create a delay by counting up to counter variable
 ** -------------------------------------------------------------------------- */
void delay_ms(int ms) {
    int i, counter;
    for (counter = 0; counter < ms; counter++) {
        for (i = 0; i < 1426; i++) {
        } //software delay 1 milliseconds
    }
}
